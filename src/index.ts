type wagerType = "win" | "place" | "final";
type operationType = "win" | "bat";
interface wager {
  palyerName: string,
  bat: number,
  wagerType?: wagerType,
  place?: number
}

class Horse {
  name: string;
  wagers: wager[] = [];
  racePlace: number = 0;
  coef: number = 0;
  /**
   * 
   * @param name     имя лошади
   * @param coef     коэффициент для ставок
   */
  constructor(name: string, coef: number) {
    this.name = name;
    this.coef = coef;
  }

  /**
   * Лошадь стартует
   */
  run(): Promise<void> {
    const horseName = this.name;
    return new Promise((resolve, reject) => {
      const time = Math.ceil(500 + Math.random() * 2500);
      setTimeout(() => {
        console.log(`${horseName} - ${time}`);
        resolve();
      }, time);
    })
  }

  /**
   * Добавляет данные о ставке на лошадь
   */
  addWager(wager: wager): void {
    this.wagers.push(wager);
  }
}

class Player {
  name: string;
  account: number;
  winSum: number = 0;
  /**
   * 
   * @param {string} name     имя игрока
   * @param {number} account  начальный счет
   */
  constructor(name: string, account: number) {
    this.name = name;
    this.account = account;
    this.winSum = 0;
  }

  /**
   * Операция со счетом
   * @param value    сумма операции
   * @param type     тип операции. "win" -зачиление призовых, "bat"- списание ставки;
   */
  updateAccount(value: number, operationType: operationType) {
    try {
      if (value < 0) {
        throw new Error(`Невозможно ${operationType === 'win' ? 'пополнить счет на' : 'списать со счета'} отрицательную сумму.`);
      }
      switch (operationType) {
        case 'win':
          this.account += value;
          this.winSum += value;
          break;

        case 'bat':
          this.account -= value;
          break;

        default:
          throw new Error('Неизвестная операция со счетом');
      }
    } catch (err) {
      console.log(err.message);
    }
  }
}

class Game {
  players: Map<string, Player> = new Map<string, Player>();
  horses: Map<string, Horse> = new Map<string, Horse>();
  constructor() {

  }

  /**
   * регистрирует ставку игрока
   * @param  palyerName                   имя игрока
   * @param  horseName                    имя лошади
   * @param  bat                          ставка                         
   * @param  wagerType                    необязательный параметр, указывает тип ставки "win" только на победу,"place" на конкретное не первое место, 'final'-что лошадь будет в тройке финалистов
   * @param place                         необязательный параметр, нужно задать если type="place"
   */
  makeBat(palyerName: string, horseName: string, bat: number, wagerType?: wagerType, place?: number) {
    const { players, horses } = this;
    const player = players.get(palyerName);
    const horse = horses.get(horseName);
    if (wagerType === 'place' && place)
      if ((place < 2 || place > horses.size)) {
        throw new Error(`Место должно быть номером от 2 до ${horses.size}.Значение "${place}" не корректно.`);
      }
    if (!horse) {
      throw new Error(`Лошадь ${horseName} не участвует в забеге.`);
    }
    if (!player) {
      throw new Error(`Игрок ${palyerName} не участвует в ставках.`);
    }
    if (player && horse) {
      if (player.account >= bat) {
        const wager: wager = { palyerName: palyerName, bat: bat, wagerType: wagerType, place: place };
        player.updateAccount(bat, 'bat');
        horse.addWager(wager);
      } else {
        throw new Error(`У ${palyerName} недостаточно средств`);
      }
    }

  }

  /**
   * Обнуляет данные о забеге
   */
  reset() {
    this.horses.forEach((horse) => {
      horse.wagers.splice(0);
      horse.racePlace = 0;
    });
    this.players.forEach((horse) => {
      horse.winSum = 0;
    });
  }

  /**
   * Показывает список имен лошадей
   */
  showHorses(): void {
    this.horses.forEach((horse) => {
      console.log(horse.name);
    });
  }

  /**
   * Добавляет игрока
   * @param name     имя игрока
   * @param account  начальный счет
   */
  addPlayer(name: string, account: number): void {
    this.players.set(name, new Player(name, account));
  }

  /**
   * Обновляет лошадей
   * @param horseCount число лошадей в забеге
   */
  announceHorses(horseCount: number): void {
    try {
      this.horses.clear();
      const possibleNameHorses: string[] = ['Буцефал', 'Жук', 'Радон', 'Абдул Альхазред', 'Рэндольф Картер', 'Харли Уоррен', 'Карл Колдун', 'Джервас Дадли', 'Моряк', 'Джо Слейтер', 'Хуан Ромеро', 'Артур Джермин', 'Бэзил Элтон', 'Куранес', 'Барзай Мудрый', 'Атал', 'Сансу', 'Таран-Иш', 'Старик в Мискатоникской долине', 'Страшный старик', 'Старик из прошлого', 'Ужасный старик', 'Томас Олни', 'Кроуфорд Тиллингаст', 'Изгой', 'Сент-Джон', 'Геррит Мартенс', 'Де ла Поэр', 'Джоэл Мэнтон', 'Этьен Руле', 'Томас Мэлоун', 'Френсис Тёрстон', 'Джон Леграсс', 'Ричард Пикман', 'Эрих Цанн', 'Герберт Уэст', 'Эмми Пирс', 'Лорд Нортем', 'Луний Габиний Капито', 'Луций Целий Руф', 'Уилбур Уэйтли', 'Старый Уэйтли', 'Генри Армитаж', 'Алберт Н. Уилмарт', 'Генри Уэнтуорт Экли', 'Нойес', 'Уильям Дайер', 'Абед Марш', 'Пфтьялйи', 'Асенат Дерби', 'Эдвард Пикман Дерби', 'Натаниэль Уингейт Пизли', 'Кларкаш-Тон', 'Джозеф Карвен', 'Кеция Мейсон', 'Бурый Дженкинс', 'Чёрный Человек', 'Роберт Харрисон Блейк', 'Эдвин М Лиллибридж', 'Кентон Дж. Стэнфилд', 'Капитан Орн', 'Умр ат-Тавил', 'Алонсо Тайпер', 'Джордж Кемпбелл'];
      if (horseCount < 3) {
        throw new Error(`Число должно быть больше 2 и меньше ${possibleNameHorses.length + 1}`);
      }
      for (let i = 0; i < horseCount; i++) {
        const index = Math.floor(possibleNameHorses.length * Math.random());
        const coef = Math.floor(Math.random() * Math.random() * 100) / 4;
        this.horses.set(possibleNameHorses[index], new Horse(possibleNameHorses[index], coef));
        possibleNameHorses.splice(index, 1);
      }
    } catch (err) {
      console.log(err.message);
    }
  }

  /**
   * Начинает забег
   */
  async startRacing(): Promise<void> {
    const arrPromise: Promise<string>[] = [];
    let raiting = 0;
    this.horses.forEach((horse) => {
      arrPromise.push(horse.run().then(() => {
        horse.racePlace = ++raiting;
        return horse.name;
      }));
    });
    const nameWinner = await Promise.race(arrPromise).then((name) => {
      return name;
    });
    await Promise.all(arrPromise);
    this.updateAccounts(nameWinner);
  }

  /**
   * Обновляет данные счетов
   * @param nameWinner имя лошади пришедшей первой
   */
  updateAccounts(nameWinner: string): void {
    const { players, horses } = this;
    horses.forEach((horse) => {
      horse.wagers.forEach((wager) => {
        const { palyerName, bat, wagerType, place } = wager;
        const player = players.get(palyerName);
        if (player)
          switch (wagerType) {
            case 'win':
              if (horse.name === nameWinner) {
                player.updateAccount(bat * (2 + horse.coef), 'win');
              }
              break;
            case 'place':
              if (place === horse.racePlace) {
                player.updateAccount(bat * (2 + horse.coef / 2), 'win');
              }
              break;
            case 'final':
              if (horse.racePlace < 4) {
                player.updateAccount(bat * (1 + horse.coef / 2), 'win');
              }
              break;
            case undefined:
              if (horse.name === nameWinner) {
                player.updateAccount(bat * 2, 'win');
              } else {
                player.updateAccount(bat, 'win');
              }
              break;
            default:

              break;
          }
      })
    });
  }
}

interface GamePlace {
  showHorses: () => void,
  showAccount: () => void,
  setWager: (horseName: string, bat: number, wagerType?: wagerType, place?: number) => void,
  startRacing: () => void,
  newGame: (numberOfHorses: number) => void,
  changeOfHorses: (numberOfHorses: number) => void
}

(window as any).game = createGamePlace();

function createGamePlace(): GamePlace {
  const game = new Game();
  game.announceHorses(4);
  // console.log(game.horses);
  game.addPlayer('Игрок', 100);
  let player = game.players.get('Игрок');

  /**
   * Показать лошадей
   */
  function showHorses() {
    game.showHorses();
  }

  function showAccount() {
    try {
      if (player)
        console.log(player.account);
      else
        throw new Error('Игрок не создан')
    } catch (err) {
      console.log(err.message)
    }
  }
  /**
   * Ставка на лошадь
   * @param  horseName                    имя лошади
   * @param  bat                          ставка                         
   * @param  type                         необязательный параметр, указывает тип ставки "win" только на победу,"place" на конкретное не первое место, 'final'-что лошадь будет в тройке финалистов
   * @param  place                        необязательный параметр, нужно задать если type="place"
   */
  function setWager(horseName: string, bat: number, wagerType?: wagerType, place?: number) {
    try {
      game.makeBat('Игрок', horseName, bat, wagerType, place);
    } catch (err) {
      console.log(err.message);
    }
  }

  /**
   * Начало забега
   */
  function startRacing() {
    game.startRacing().then(() => {
      try {
        if (player) {
          let text = `Текущий счет ${player.account}`;
          //console.log(player);
          if (player.winSum) {
            text = `Вы выиграли ${player.winSum}, ${text.toLowerCase()}`;
          }
          console.log(text);
          game.reset();
        }
        else
          throw new Error('Игрок не создан')
      } catch (err) {
        console.log(err.message)
      }
    });
  }

  /**
   * Смена лошадей
   * @param numberOfHorse Число лошадей
   */
  function changeOfHorses(numberOfHorses: number) {
    game.announceHorses(numberOfHorses);
  }

  /**
   * Новая игра
   * @param numberOfHorse Число лошадей
   */
  function newGame(numberOfHorses: number) {
    game.announceHorses(numberOfHorses ? numberOfHorses : 4);
    game.players.clear();
    game.addPlayer('Игрок', 100);
    player = game.players.get('Игрок');
  }

  return {
    showHorses: showHorses,
    showAccount: showAccount,
    setWager: setWager,
    startRacing: startRacing,
    newGame: newGame,
    changeOfHorses: changeOfHorses
  };
}